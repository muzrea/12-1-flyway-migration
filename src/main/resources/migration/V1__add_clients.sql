CREATE TABLE clients (
    id BIGINT,
    full_name VARCHAR(128),
    abbreviation VARCHAR(6),
    PRIMARY KEY (id),
    type VARCHAR(100)
)