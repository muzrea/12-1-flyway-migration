CREATE TABLE contracts (
    id BIGINT,
    name VARCHAR(255),
    client_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (client_id) REFERENCES clients(id)
)