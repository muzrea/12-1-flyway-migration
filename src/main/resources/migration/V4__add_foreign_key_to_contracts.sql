ALTER TABLE contracts
ADD service_id BIGINT;
ALTER TABLE contracts
ADD FOREIGN KEY (service_id) REFERENCES service(id);