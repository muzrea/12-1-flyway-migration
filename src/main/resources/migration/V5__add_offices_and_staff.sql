CREATE TABLE offices(
    id BIGINT,
    country VARCHAR(100),
    city VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE staff(
    id BIGINT,
    first_name VARCHAR(128),
    last_name VARCHAR(128),
    office_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (office_id) REFERENCES offices(id)
);