CREATE TABLE contract_service_offices(
    id BIGINT,
    contract_id BIGINT,
    office_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (contract_id) REFERENCES contracts(id),
    FOREIGN key (office_id) REFERENCES offices(id)
);